#pragma once

#include <stdexcept>

int BinPow(int a, int64_t b, int c) {
    if (b == 0) {
        return 1;
    }
    int64_t d = BinPow(a, b / 2, c);
    if (b % 2 == 0) {
        return static_cast<int>(((d % c) * (d % c)) % c);
    } else {
        return static_cast<int>((((a % c) * (d % c)) % c * (d % c)) % c);
    }
}
