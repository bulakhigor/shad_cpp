#pragma once

#include <vector>
#include <stdexcept>
#include <algorithm>
#include <unordered_map>
#include <iostream>

inline int64_t CountPairs(const std::vector<int>& data, int x) {
    std::unordered_map<int, int> result;
    int64_t res;
    res = 0;
    for (const auto& item : data) {
        result[item]++;
    }

    for (const auto& item : data) {
        std::cout << item << '\n';
    }

    for (const auto& [key, value] : result) {
        auto it = result.find(x - key);
        if (it != result.end()) {
            res += (value * it->second);
        }
    }
    return res / 2;
}
