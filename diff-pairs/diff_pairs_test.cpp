#include <catch.hpp>

#include <diff_pairs.h>
#include <vector>

TEST_CASE("Easey test_1") {
    const std::vector<int>& data = {3, 3, 3, 7, 7, 7, 7, 7};
    REQUIRE(15 == CountPairs(data, 10));
}

TEST_CASE("Easey test_2") {
    const std::vector<int>& data = {3, 6, 1, 9, 3, 0, 3, 2};
    REQUIRE(3 == CountPairs(data, 12));
}

TEST_CASE("Easey test_3") {
    const std::vector<int>& data = {1, 3, 2, 0, 4};
    REQUIRE(2 == CountPairs(data, 5));
}

TEST_CASE("Easey test_4") {
    const std::vector<int>& data = {1, 1, 1};
    REQUIRE(3 == CountPairs(data, 2));
}

//TEST_CASE("Middle tests") {
//    std::mt19937 gen(time(0));
//    std::uniform_int_distribution<int> dist(1, 1500);
//    size_t data_size = 1500;
//    std::vector<int> data(data_size);
//    for (size_t i = 0; i < data_size; ++i) {
//        data.push_back(dist(gen));
//    }
//    REQUIRE(3182 == CountPairs(data, 345));
//}