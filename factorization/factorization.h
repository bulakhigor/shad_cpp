#pragma once

#include <utility>
#include <vector>
#include <stdexcept>
#include <iostream>

std::vector<std::pair<int64_t, int>> Factorize(int64_t x) {
    std::vector<std::pair<int64_t, int>> res;
    int64_t delimeter = 2;
    int counter = 0;
    for (;delimeter <= sqrt(x); ++delimeter) {
        while (x % delimeter == 0) {
            ++counter;
            x /= delimeter;
        }
        if (counter > 0) {
            res.emplace_back(delimeter, counter);
        }
        if (x % delimeter != 0) {
            counter = 0;
        }
    }
    if (x != 1) {
        res.emplace_back(x, ++counter);
    }
    return res;
}
