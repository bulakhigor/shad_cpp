#pragma once

#include <vector>
#include <stdexcept>
#include <iostream>

void FilterEven(std::vector<int>* data) {
    size_t pos = 0;
    for (auto& index : *data) {
        if (index % 2 == 0) {
            auto tmp = index;
            index = (*data)[pos];
            (*data)[pos++] = tmp;
        }
    }
    data->resize(pos);
}
