#pragma once

#include <string>
#include <stdexcept>

char ToAscii(int a) {
    char symbol = a + 48;
    return symbol;
}

int ToInt(char a) {
    int res = a - 48;
    return res;
}

std::string Swap(std::string& num) {
    size_t j = 0;
    size_t k = num.size() - 1;
    for (; j <= k;) {
        if (j == k) {
            break;
        }
        std::swap(num[j], num[k]);
        j++;
        k--;
    }
    return num;
}

inline std::string LongSum(const std::string& a, const std::string& b) {
    size_t index = 0;
    std::string first = a;
    std::string second = b;
    std::string res;
    Swap(first);
    Swap(second);
    bool next = false;
    while (first.size() < second.size() ? index < first.size() : index < second.size()) {
        int result = ToInt(first[index]) + ToInt(second[index]);
        if (next) {
            result += 1;
            next = false;
        }
        if (result >= 10) {
            next = true;
            int tmp = result % 10;
            res += ToAscii(tmp);
        } else {
            res += ToAscii(result);
        }

        index++;
    }

    std::string remain = first.size() < second.size() ? second : first;

    for (size_t i = index; i < remain.size(); ++i) {
        int result = ToInt(remain[i]);
        if (next) {
            result += 1;
            next = false;
        }
        if (result >= 10) {
            next = true;
            int tmp = result % 10;
            res += ToAscii(tmp);
        } else {
            res += ToAscii(result);
        }
    }

    if (next) {
        res += ToAscii(1);
    }

    Swap(res);

    return res;
}