#include <catch.hpp>

#include <long_sum.h>

#include <algorithm>
#include <vector>

TEST_CASE("Your test") {
    REQUIRE("4" == LongSum("2", "2"));
}

TEST_CASE("Double numeric") {
    REQUIRE("48" == LongSum("19", "29"));
}

TEST_CASE("Big numbers") {
    REQUIRE("1021203" == LongSum("164879", "856324"));
}

TEST_CASE("Big number with 0") {
    REQUIRE("26585" == LongSum("164", "26421"));
    REQUIRE("20200" == LongSum("20000", "200"));
}

TEST_CASE("Big number with 9") {
    REQUIRE("10000212" == LongSum("748", "9999464"));
}

TEST_CASE("Huge numbers") {
    REQUIRE("13429148140567020" == LongSum("4715893247825792", "8713254892741228"));
    REQUIRE("78877333352342445860854" == LongSum("78876876475465458779867", "456876876987080987"));
    REQUIRE("3498572034602572479135455923730829549333273" == LongSum("3498572034598724958740598720234958739458739", "3847520394857203495870809874534"));
    std::string first_number;
    std::string result = "1";
    for (int i = 0; i < 1000000; ++i) {
        first_number += '9';
    }
    for (int i = 0; i < 999957; ++i) {
        result += '0';
    }
    result += "3498572034602572479135455923730829549333273";
    REQUIRE(result == LongSum(first_number, "3498572034602572479135455923730829549333274"));
}