#pragma once

#include <vector>
#include <stdexcept>

size_t Factorial(size_t n) {
    size_t res = n;
    for (size_t i = n - 1; i > 0; --i) {
        res *= i;
    }
    return res;
}

void Swap(std::vector<int>& a, size_t i, size_t j) {
    int temp = a[i];
    a[i] = a[j];
    a[j] = temp;
}

bool NextStep(std::vector<int>& a, int n) {
    int it = n - 2;
    while (it != -1 && a[it] >= a[it + 1]) {
        it--;
    }
    if (it == -1) {
        return false;
    }
    int j = n - 1;
    while (a[it] >= a[j]) {
        j--;
    }
    Swap(a, static_cast<size_t>(it), static_cast<size_t>(j));
    int l = it + 1;
    int r = n - 1;
    while (l < r) {
        Swap(a, static_cast<size_t>(l++), static_cast<size_t>(r--));
    }
    return true;
}

std::vector<std::vector<int>> GeneratePermutations(size_t len) {
    std::vector<int> numbers;
    std::vector<std::vector<int>> result;
    numbers.reserve(len);
    result.reserve(Factorial(len));
    for (size_t i = 0; i < len; ++i) {
        numbers.push_back(static_cast<int>(i));
    }
    result.push_back(numbers);
    while (NextStep(numbers, static_cast<int>(len))) {
        result.push_back(numbers);
    }
    return result;
}