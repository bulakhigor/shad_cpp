#pragma once

#include <string>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <stack>

std::vector<std::string> Split(const std::string& string, const std::string& delimiter = " ") {
    size_t space = string.find(delimiter);
    size_t start = 0;
    std::vector<std::string> result;
    if (string.empty()) {
        result.push_back("");
        return result;
    }
    if (space != std::string::npos) {
        while (space != std::string::npos) {
            if (string == delimiter) {
                result.push_back("");
                result.push_back("");
                break;
            }
            std::string current = string.substr(start, space);
            result.push_back(current);
            start += space + delimiter.size();
            current = string.substr(start, string.size() - start + 1);
            space = current.find(delimiter);
            if (space == std::string::npos && start <= string.size()) {
                result.push_back(string.substr(start, std::string::npos));
                break;
            }
        }
    } else {
        result.push_back(string.substr(start, std::string::npos));
    }
    return result;
}

int EvaluateExpression(const std::string& expression) {
    std::stack<int> data;
    std::vector<std::string> v = Split(expression);
    int result = 0;
    int first = 0;
    int second = 0;
    for (const auto& i : v) {
        if (i != "+" && i != "*" && i != "-") {
            int t = stoi(i);
            data.push(t);
        } else {
            if (i == "+") {
                first = data.top();
                data.pop();
                second = data.top();
                data.pop();
                result = first + second;
                data.push(result);
            } else if (i == "*") {
                first = data.top();
                data.pop();
                second = data.top();
                data.pop();
                result = first * second;
                data.push(result);
            } else if (i == "-") {
                first = data.top();
                data.pop();
                second = data.top();
                data.pop();
                result = second - first;
                data.push(result);
            }
        }
    }
    return data.top();
}
