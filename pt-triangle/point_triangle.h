#pragma once

#include <stdexcept>
#include <cmath>
#include <iostream>

struct Point {
    int x, y;
};

struct Triangle {
    Point a, b, c;
};

int triangle_area_2 (int x1, int y1, int x2, int y2, int x3, int y3) {
	return (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1);
}

double triangle_area (int x1, int y1, int x2, int y2, int x3, int y3) {
	return abs (triangle_area_2 (x1, y1, x2, y2, x3, y3)) / 2.0;
}

bool clockwise (int x1, int y1, int x2, int y2, int x3, int y3) {
	return triangle_area_2 (x1, y1, x2, y2, x3, y3) < 0;
}

bool counter_clockwise (int x1, int y1, int x2, int y2, int x3, int y3) {
	return triangle_area_2 (x1, y1, x2, y2, x3, y3) > 0;
}

bool tolerably_equal (int x, int y, double eps) {
    return abs(x - y) < eps;
}

inline bool IsPointInTriangle(const Triangle& t, const Point& pt) {
    double S1 = triangle_area(t.a.x, t.a.y, t.b.x, t.b.y, pt.x, pt.y);
    double S2 = triangle_area(t.b.x, t.b.y, t.c.x, t.c.y, pt.x, pt.y);
    double S3 = triangle_area(t.c.x, t.c.y, t.a.x, t.a.y, pt.x, pt.y);

    double CS = S1 + S2 + S3;
    double S = triangle_area(t.a.x, t.a.y, t.b.x, t.b.y, t.c.x, t.c.y);

    if (tolerably_equal(CS, S, 0.00001)) {
        return true;
    }
    return false;
}
