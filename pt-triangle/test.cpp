#include <catch.hpp>
#include <point_triangle.h>

TEST_CASE("Your test") {
    Point p;
    p.x = 4;
    p.y = 4;

    Point a;
    a.x = 3;
    a.y = 2;

    Point b;
    b.x = 7;
    b.y = 3;

    Point c;
    c.x = 5;
    c.y = 8;
    Triangle t;
    t.a = a;
    t.b = b;
    t.c = c;

    REQUIRE(true == IsPointInTriangle(t, p));
}


TEST_CASE("Test 1") {
    Point p;
    p.x = 3;
    p.y = 9;

    Point a;
    a.x = 3;
    a.y = 2;

    Point b;
    b.x = 7;
    b.y = 3;

    Point c;
    c.x = 5;
    c.y = 8;

    Triangle t;
    t.a = a;
    t.b = b;
    t.c = c;
    REQUIRE(false == IsPointInTriangle(t, p));
}