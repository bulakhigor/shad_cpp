#pragma once

#include <stdexcept>
#include <cmath>
#include <iostream>
#include <iomanip>

enum RootsCount { kZero, kOne, kTwo, kInf };

struct Roots {
    RootsCount count;
    double first, second;
};

Roots SolveQuadratic(int a, int b, int c) {
    Roots r;
    if (a == 0 && b == 0 && c == 0) {
        r.count = kInf;
    } else if (a == 0 && b != 0 && c != 0) {
        r.count = kOne;
        r.first = static_cast<double>(c) * -1 / b;
    } else if (a == 0 && b == 0 && c != 0) {
        r.count = kZero;
    } else if (a != 0 && b == 0 && c != 0) {
        if (c > 0) {
            r.count = kZero;
        } else if (c < 0) {
            r.count = kTwo;
            r.first = -1 * sqrt(c);
            r.second = sqrt(c);
        }
    } else if (a != 0 && b == 0 && c == 0) {
        r.count = kOne;
        r.first = 0;
    } else if (a != 0 && b != 0 && c == 0) {
        r.count = kTwo;
        r.first = -1 * b / a;
        r.second = 0;
    } else if (a != 0 && b != 0 && c != 0) {
        double D = b * b - 4 * a * c;
        if (D > 0) {
            r.count = kTwo;
            auto x1 = (-1 * static_cast<double>(b) + sqrt(D)) / (2 * a);
            auto x2 = (-1 * static_cast<double>(b) - sqrt(D)) / (2 * a);
            if (x1 < x2) {
                r.first = x1;
                r.second = x2;
            } else {
                r.second = x1;
                r.first = x2;
            }
        } else if (D == 0) {
            r.count = kOne;
            r.first = (-1 * static_cast<double>(b)) / (2 * a);
        } else if (D < 0) {
            r.count = kZero;
        }
    }
    return r;
}
