#pragma once

#include <vector>
#include <stdexcept>
#include <iostream>

std::vector<int> Range(int from, int to, int step = 1) {
    std::vector<int> result;
    int range;
    if (from == to || from == to * -1 || to == from * -1 || step == 0) {
        return result;
    } else if (step > 0) {
        if (from > to) {
            return result;
        }
        range = ((to - 1 - from) / step) + 1;
        result.reserve(range);
        for (int i = from; i < to; i += step) {
            result.emplace_back(i);
        }
    } else if (step < 0) {
        if (from < to) {
            return result;
        }
        range = ((from - to - 1) / step * -1) + 1;
        result.reserve(range);
        for (int i = from; i > to; i += step) {
            result.emplace_back(i);
        }
    }
    return result;
}
