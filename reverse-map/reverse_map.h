#pragma once

#include <string>
#include <unordered_map>
#include <stdexcept>

std::unordered_map<int, std::string> ReverseMap(std::unordered_map<std::string, int> map) {
    std::unordered_map<int, std::string> result;
    for (auto& item : map) {
        result[std::move(item.second)] = std::move(item.first);
    }

    return result;
}
