#pragma once

#include <stdexcept>
#include <vector>
#include <iostream>


void Swap(int& l, int& j) {
    int temp = l;
    l = j;
    j = temp;
}


void Rotate(std::vector<int> *data, size_t shift) {
    size_t size = (*data).size();
    if (!(*data).empty()) {
        shift %= size;
    }

    auto first = (*data).begin();
    auto middle = first + shift;
    auto last = (*data).end() - 1;

    auto next = middle;
    while (first != next) {
        Swap(*first++, *next++);
        if (next == last + 1) {
            next = middle;
        } else if (first == middle) {
            middle = next;
        }
    }
}
