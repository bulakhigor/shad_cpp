#include <iostream>
#include <initializer_list>

class Array {
public:
    explicit Array(size_t size) {
        data_ = new int[size]{};
        size_ = size;
    }

    Array(const Array& rhs) {
        data_ = new int[rhs.size_];
        for (size_t i = 0; i < rhs.size_; ++i) {
            data_[i] = rhs.data_[i];
        }
        size_ = rhs.size_;
    }

    Array& operator=(Array rhs) {
        Swap(rhs);
        return *this;
    }

    Array(Array&& rhs) {
        data_ = rhs.data_;
        size_ = rhs.size_;
        rhs.data_ = nullptr;
    }

    Array(std::initializer_list<int> list) {
        data_ = new int[list.size()];
        size_ = list.size();
        size_t i = 0;
        for (int cur : list) {
            data_[i++] = cur;
        }
    }

    ~Array() {
        delete[] data_;
    }

    void Swap(Array& rhs) {
        std::swap(data_, rhs.data_);
        std::swap(size_, rhs.size_);
    }
private:
    int *data_;
    size_t size_;
};

int main() {
    return 0;
}