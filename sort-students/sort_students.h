#pragma once

#include <vector>
#include <stdexcept>

struct Student {
    std::string name, surname;
    int year, month, day;
};

enum class SortType { kByName, kByDate };

bool check_name(const Student& lhs, const Student& rhs) {
    if (std::tie(lhs.surname, lhs.name) == std::tie(rhs.surname, rhs.name)) {
        return std::tie(lhs.year, lhs.month, lhs.day) < std::tie(rhs.year, rhs.month, rhs.day);
    }
    return std::tie(lhs.surname, lhs.name) < std::tie(rhs.surname, rhs.name);
}

bool check_date(const Student& lhs, const Student& rhs) {
    if (std::tie(lhs.year, lhs.month, lhs.day) == std::tie(rhs.year, rhs.month, rhs.day)) {
        return std::tie(lhs.surname, lhs.name) < std::tie(rhs.surname, rhs.name);
    }
    return std::tie(lhs.year, lhs.month, lhs.day) < std::tie(rhs.year, rhs.month, rhs.day);
}

bool operator==(const Student& lhs, const Student& rhs) {
    return std::tie(lhs.surname, lhs.name, lhs.year, lhs.month, lhs.day) == std::tie(
        rhs.surname, rhs.name, rhs.year, rhs.month, rhs.day);
}

inline void SortStudents(std::vector<Student> *students, SortType sort_type) {
    if (sort_type == SortType::kByDate) {
        std::sort((*students).begin(), (*students).end(), check_date);
    } else if (sort_type == SortType::kByName) {
        std::sort((*students).begin(), (*students).end(), check_name);
    }
}