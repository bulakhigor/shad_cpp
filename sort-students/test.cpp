#include <catch.hpp>
#include <sort_students.h>

TEST_CASE("Sort by Date") {
    std::vector<Student> students;

    Student st_1;
    Student st_2;
    Student st_3;
    Student st_4;
    Student st_5;
    
    st_1.name = "Igor";
    st_1.surname = "Bulakh";
    st_1.year = 1987;
    st_1.month = 1;
    st_1.day = 7;
    students.emplace_back(st_1);

    st_2.name = "Valeriy";
    st_2.surname = "Dimchenko";
    st_2.year = 1963;
    st_2.month = 6;
    st_2.day = 13;
    students.emplace_back(st_2);

    st_3.name = "Julia";
    st_3.surname = "Bulakh";
    st_3.year = 1993;
    st_3.month = 7;
    st_3.day = 15;
    students.emplace_back(st_3);

    st_4.name = "Allah";
    st_4.surname = "Volianskaya";
    st_4.year = 1968;
    st_4.month = 12;
    st_4.day = 26;
    students.emplace_back(st_4);

    st_5.name = "Igor";
    st_5.surname = "Bulakh";
    st_5.year = 1993;
    st_5.month = 7;
    st_5.day = 15;
    students.emplace_back(st_5);

    std::vector<Student> *stdn = &students;

    SortStudents(stdn, SortType::kByDate);
    std::vector<Student> result = {st_2, st_4, st_1, st_5, st_3};

    REQUIRE(result == students);
}


TEST_CASE("Sort by name") {
    std::vector<Student> students;

    Student st_1;
    Student st_2;
    Student st_3;
    Student st_4;
    Student st_5;
    
    st_1.name = "Yyyy";
    st_1.surname = "Zzzz";
    st_1.year = 1987;
    st_1.month = 1;
    st_1.day = 7;
    students.emplace_back(st_1);

    st_2.name = "Xxxx";
    st_2.surname = "Yyyy";
    st_2.year = 1987;
    st_2.month = 1;
    st_2.day = 7;
    students.emplace_back(st_2);

    st_3.name = "Xxxx";
    st_3.surname = "Yyyy";
    st_3.year = 1986;
    st_3.month = 1;
    st_3.day = 7;
    students.emplace_back(st_3);

    st_4.name = "Wwww";
    st_4.surname = "Xxxx";
    st_4.year = 1985;
    st_4.month = 1;
    st_4.day = 7;
    students.emplace_back(st_4);

    st_5.name = "Vvvv";
    st_5.surname = "Wwww";
    st_5.year = 1997;
    st_5.month = 7;
    st_5.day = 15;
    students.emplace_back(st_5);

    std::vector<Student> *stdn = &students;

    SortStudents(stdn, SortType::kByName);
    std::vector<Student> result = {st_5, st_4, st_3, st_2, st_1};

    REQUIRE(result == students);
}
