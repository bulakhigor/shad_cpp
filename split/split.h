#pragma once

#include <string>
#include <stdexcept>
#include <algorithm>
#include <vector>

std::vector<std::string> Split(const std::string& string, const std::string& delimiter = " ") {
    size_t space = string.find(delimiter);
    size_t start = 0;
    std::vector<std::string> result;
    if (string.empty()) {
        result.push_back("");
        return result;
    }
    if (space != std::string::npos) {
        while (space != std::string::npos) {
            if (string == delimiter) {
                result.push_back("");
                result.push_back("");
                break;
            }
            std::string current = string.substr(start, space);
            result.push_back(current);
            start += space + delimiter.size();
            current = string.substr(start, string.size() - start + 1);
            space = current.find(delimiter);
            if (space == std::string::npos && start <= string.size()) {
                result.push_back(string.substr(start, std::string::npos));
                break;
            }
        }
    } else {
        result.push_back(string.substr(start, std::string::npos));
    }
    return result;
}
