
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * compiled with:
 * gcc -m32 -O0 -fno-stack-protector stack2.c -o stack2
 */

void shell()
{
	printf("You did it.\n");
	system("/bin/sh");
}

int main(int argc, char** argv)
{
	if(argc != 1)
	{
		printf("usage:\n%s\n", argv[0]);
		return EXIT_FAILURE;
	}

        char input[1024];
        bzero(input, sizeof(input));
        fgets(input, 1023, stdin);

        struct {
                char buf[15];
                int set_me;
        } l;
        l.set_me = 0;
        
        printf("You input %s", input);
	strcpy(l.buf, input);

	if(l.set_me == 0xdeadbeef)
	{
		shell();
	}
	else
	{
		printf("Not authenticated.\nset_me was 0x%0x\n", l.set_me);
	}

	return EXIT_SUCCESS;
}
