#pragma once

#include <vector>
#include <set>

std::vector<int> Unique(const std::vector<int>& data) {
    std::set<int> unique_data(data.begin(), data.end());
    return {unique_data.begin(), unique_data.end()};
}
