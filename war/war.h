#pragma once

#include <array>
#include <stdexcept>
#include <iostream>
#include <queue>

enum Winner { kFirst, kSecond, kNone };

struct GameResult {
    Winner winner;
    int turn;
};

GameResult SimulateWarGame(const std::array<int, 5>& first_deck,
                           const std::array<int, 5>& second_deck) {
    std::queue<int> current_first_deck;
    std::queue<int> current_second_deck;
    GameResult result = GameResult();
    int turn_count = 0;

    for (size_t i = 0; i < first_deck.size(); ++i) {
        if (first_deck[i] == 0 && second_deck[i] == 9) {
            current_first_deck.push(first_deck[i]);
            current_first_deck.push(second_deck[i]);
        } else if (first_deck[i] == 9 && second_deck[i] == 0) {
            current_second_deck.push(first_deck[i]);
            current_second_deck.push(second_deck[i]);
        } else if (first_deck[i] > second_deck[i]) {
            current_first_deck.push(first_deck[i]);
            current_first_deck.push(second_deck[i]);
        } else if (first_deck[i] < second_deck[i]) {
            current_second_deck.push(first_deck[i]);
            current_second_deck.push(second_deck[i]);
        }
        ++turn_count;
    }

    if (!current_first_deck.empty() && current_first_deck.size() < 10) {
        while (!current_first_deck.empty() || !current_second_deck.empty()) {
            if (current_first_deck.empty() || current_second_deck.empty()) {
                break;
            }
            if (current_first_deck.front() == 0 && current_second_deck.front() == 9) {
                current_first_deck.push(current_first_deck.front());
                current_first_deck.push(current_second_deck.front());
            } else if (current_first_deck.front() == 9 && current_second_deck.front() == 0) {
                current_second_deck.push(current_first_deck.front());
                current_second_deck.push(current_second_deck.front());
            } else if (current_first_deck.front() > current_second_deck.front()) {
                current_first_deck.push(current_first_deck.front());
                current_first_deck.push(current_second_deck.front());
            } else if (current_first_deck.front() < current_second_deck.front()) {
                current_second_deck.push(current_first_deck.front());
                current_second_deck.push(current_second_deck.front());
            }
            current_first_deck.pop();
            current_second_deck.pop();
            ++turn_count;
        }
    }

    if (turn_count > 1000000) {
        result.winner = kNone;
    } else if (current_first_deck.empty()) {
        result.winner = kSecond;
        result.turn = turn_count;
    } else if (current_second_deck.empty()) {
        result.winner = kFirst;
        result.turn = turn_count;
    }
    return result;
}
