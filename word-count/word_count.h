#pragma once

#include <string>
#include <stdexcept>
#include <unordered_set>
#include <vector>
#include <cctype>

std::string Strtolower(const std::string& str) {
    std::string new_str;
    for (size_t i = 0; i < str.size(); ++i) {
        new_str += std::tolower(str[i]);
    }
    return new_str;
}

int DifferentWordsCount(const std::string& string) {
    std::unordered_set<std::string> result;
    std::string word;
    for (size_t i = 0; i < string.size(); ++i) {
        if (std::isalnum(string[i])) {
            word += string[i];
        } else {
            if (ispunct(string[i])) {
                if (!word.empty()) {
                    result.insert(word);
                } else {
                    continue;
                }
            }
            if (!word.empty() && word != " ") {
                result.insert(word);
            }
            word = "";
        }
    }
    if (!word.empty()) {
        result.insert(word);
    }
    return static_cast<int>(result.size());
}
